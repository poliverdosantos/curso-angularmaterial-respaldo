import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// importamos un Observable y el Menu de interfaces
import { Observable } from 'rxjs';
import { MenuI } from '../interfaces/menu.interface';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor( private http: HttpClient ) { }

  /* 
    El observable es algo que siempre va estar escuchando y si le mandas lo que tienes que escuchar
  */
  // usamos un observable para que reciba los parametros de la interface MenuI
  getMenu(): Observable<MenuI[]> {
    // obtenemos los datos de un json y lo hacemos mediante la interfaz ya que tienen los mismos parámetros
    return this.http.get<MenuI[]>('./assets/data/menu.json');
  }

}
