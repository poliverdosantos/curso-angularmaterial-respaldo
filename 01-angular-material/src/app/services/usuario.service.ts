import { Injectable } from '@angular/core';

// importamos la interfaz de PeriodicElemento
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  // la información siempre debe estar dentro de la clase a eso le llamamos refactorización y en un service, ya sean datos estáticos o de un end point
  listUsuarios: UsuarioDataI[] = [
    {usuario: 'jperez', nombre: 'Juan', apellido: 'Perez', sexo: 'Masculino'},
    {usuario: 'mgomez', nombre: 'Maria', apellido: 'Gomez', sexo: 'Masculino'},
    {usuario: 'ngarcia', nombre: 'Nilda', apellido: 'Garcia', sexo: 'Femenino'},
    {usuario: 'kliop', nombre: 'Kevin', apellido: 'Liop', sexo: 'Masculino'},
    {usuario: 'hmarino', nombre: 'Hernan', apellido: 'Marino', sexo: 'Masculino'},
    {usuario: 'mmendizabal', nombre: 'Magdiel', apellido: 'Mendizabal', sexo: 'Femenino'}
  
  ];

  constructor() { }

  // método para obtener usuario
  getUsuario(): UsuarioDataI[] {
    // slice retorna una copia del array
    return this.listUsuarios.slice();
  }

  eliminarUsuario(usuario: string) {
    this.listUsuarios = this.listUsuarios.filter( data => {
      return data.usuario !== usuario;
    })
  }

  // agregamos un array al inicio del array
  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
  }

  // los id son los nombres de usuario de tipo cadena
  buscarUsuario(id: string): UsuarioDataI {
    // o retorna un json {} vacio, elemento es cada json de cada elemento
    return this.listUsuarios.find(element => element.usuario === id) || {} as UsuarioDataI;
  }

  modificarUsuario(user: UsuarioDataI){
   this.eliminarUsuario(user.usuario);
   this.agregarUsuario(user);
  }
    

}
