import { Component, OnInit } from '@angular/core';

// importamos el servicio de menu y la interface de menu
import { MenuService } from 'src/app/services/menu.service';
import { MenuI } from 'src/app/interfaces/menu.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  menu: MenuI[] = [];


  constructor( private _menuService: MenuService ) { }

  // cargamos los metodos en el ngOnInit para que se carguen después del constructor
  ngOnInit(): void {
    // cargamos los métodos
    this.cargarMenu();
  }

  cargarMenu(): void {
    /* 
      Necesitas suscribirte al con 'subscribe' para que dispares y puedas hacer operaciones con los datos que estas obteniendo, trabaja en conjunto con el Observable
    */
    // obtenemos la función getmenu de menu.services que este a su vez trae un json
    this._menuService.getMenu().subscribe( data => {
      console.log(data);
      this.menu = data; // guardamos los datos del json en el array menu
    })
  }

}
