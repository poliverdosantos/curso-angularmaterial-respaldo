import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Formularios reactivos
// import { ReactiveFormsModule } from '@angular/forms';

// importamos todo lo de Angular Material
import { SharedModule } from './components/shared/shared.module';

// importamos Componentes
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // ReactiveFormsModule, esto ya tenemos en shared.module y por eso es inecesario ya que solamente importa y no exporta como en shared.module
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
